package main

import (
	"fmt"
)

//find second largest element in an array
func test1() {

	var firstLargest int = 0
	var secondLargest int = 0

	var nums = []int{43, 11, 6, 81, 34}
	firstLargest = nums[0]
	for j := 1; j < len(nums); j++ {
		if firstLargest < nums[j] {
			secondLargest = firstLargest
			firstLargest = nums[j]
		} else if secondLargest < nums[j] {
			secondLargest = nums[j]
		}

	}

	fmt.Println("Second largest element is: ", secondLargest)

}
