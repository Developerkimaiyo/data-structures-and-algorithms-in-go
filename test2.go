package main

import (
	"fmt"
)

//returns the duplicate number in a list of integers

func test2() {

	var nums = []int{4, 1, 6, 8, 4}
	duplicate := make(map[int]bool)
	for i := 0; i < len(nums); i++ {
		if duplicate[nums[i]] {
			fmt.Println("Duplicate number is: ", nums[i])
		} else {
			duplicate[nums[i]] = true
		}
	}
}
