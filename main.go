package main

import (
	"fmt"
)

// returns an array containing their corresponding ranks
func test6() {
	var n = []int{34, 5, 8, 90}
	for j := 0; j < len(n); j++ {
		r := 1
		s := 1
		for k := 0; k < len(n); k++ {
			if (j != k) && (n[k] < n[j]) {
				r += 1
			}
			if (j != k) && (n[k] == n[j]) {
				s += 1
			}
		}
		fmt.Print(float32(float32(r) + float32(s-1)/(float32(2))))
		fmt.Print(` `)
	}
}

func main() {
	test1()
	test2()
	test3()
	test4()
	test5()
	test6()

}
