package main

import (
	"fmt"
	"strings"
)

//return a new array with the names that start with J

func Filter(names []string, f func(string) bool) []string {
	filtered := make([]string, 0)
	for _, v := range names {
		if f(v) {
			filtered = append(filtered, v)
		}
	}
	return filtered
}

func test4() {

	var names = []string{"Joseph", "Clinton", "Justin", "Mark"}

	filtered := Filter(names, func(word string) bool {
		return strings.HasPrefix(word, "J")
	})
	fmt.Println(filtered)

}
