package main

import (
	"fmt"
)

//return the reverse of string.

func test3() {
	str := "Sendy"
	str_byte := []rune(str)
	for i, j := 0, len(str_byte)-1; i < j; i, j = i+1, j-1 {

		str_byte[i], str_byte[j] = str_byte[j], str_byte[i]
	}

	fmt.Println(string(str_byte))

}
