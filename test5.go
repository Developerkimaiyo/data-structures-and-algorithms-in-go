package main

import (
	"fmt"
)

//displays the stack(s) contained in both arrays.

func Related(arr1 []string, arr2 []string) {
	for _, v := range arr1 {
		for _, y := range arr2 {
			if v == y {
				fmt.Println(v)
			} else {
				continue
			}
		}
	}

}

func test5() {

	var frontend = []string{"CSS", "HTML", "JavaScript", "Vue.js"}
	var backend = []string{"Java", "Go", "JavaScript", "PHP"}

	Related(frontend, backend)

}
